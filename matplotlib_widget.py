# embedding_in_qt4.py --- Simple Qt4 application embedding matplotlib canvases
#
# Copyright (C) 2005 Florent Rougon
#               2006 Darren Dale
#
# This file is an example program for matplotlib. It may be used and
# modified with no restriction; raw copies as well as modified versions
# may be distributed without limitation.

import sys, os, random
from numpy import arange, sin, pi
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from UI_graphe import Ui_Graphe

progname = os.path.basename(sys.argv[0])
progversion = "0.1"


class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self,  parent, donnees, traitement, dates, width=0, height=0, dpi=100, cliquable=False, titre=""):
        self.fig = Figure(figsize=(width, height), dpi=dpi )
        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(
            self, QSizePolicy.Expanding, QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.axes = self.fig.add_subplot(111)
        a = self.axes.set_axis_off()
        # We want the axes cleared every time plot() is called
        self.axes.axison = False
        self.axes.axis('off')
        self.axes._hold=False

        self.cliquable=cliquable
        self.donnees=donnees
        self.traitement=traitement
        self.dates=dates
        self.titre=titre

        self.plot(donnees, traitement, dates)


    def mouseReleaseEvent(self, event):
        if self.cliquable:
            self.fils=QDialog()
            self.fils.ui=Ui_Graphe()
            self.fils.ui.setupUi(self.fils)
            self.fils.setWindowTitle(self.tr(self.titre))
            self.fils.show()
            ratio=2
            w=3*ratio
            h=5*ratio
            d=90/ratio
            self.fils.canvas=MyMplCanvas(self.fils.ui.grapheLabel,self.donnees, self.traitement, self.dates, width=w, height=h, dpi=d, cliquable=False, titre=self.titre)
            self.fils.canvas.show()
        
    def sizeHint(self):
        w, h = self.get_width_height()
        #print "w, h", w, h
        return QtCore.QSize(w, h)

    def minimumSizeHint(self):
        return QtCore.QSize(10, 10)
    
    def plot(self,donnees, traitement, dates):
        d=[]
        for dd in donnees:
            d.append(traitement(dd))
        self.axes.plot(dates,d)

