# -*- coding: utf-8 -*-

import os.path

class repertoire:
    def __init__(self, chemin):
        self.chemin0=os.path.abspath(chemin)
        if os.path.isfile(self.chemin0):
            self.chemin0=os.path.dirname(self.chemin0)
        
    def chemin(self,choix="defaut"):
        if choix=="defaut":
            return self.chemin0
        if choix=="textures":
            for d in [self.chemin0, "/usr/share/pysatellites"]:
                if os.path.exists(d+'/images/earth-clouds.jpg'):
                    return d
            print ("erreur : pas de répertoire des planètes")
            print ("=== Il manque les textures de Xplanet-images ===")
            raise(IOError)
        elif os.path.isdir(os.path.join(self.chemin0,choix)):
            return os.path.join(self.chemin0,choix)
        else:
            raise(IOError)
    def fichier(self,*elementsDeChemin):
        f=self.chemin0
        for e in elementsDeChemin:
            f=os.path.join(f,e)
        return f
        
