# -*- coding: utf-8 -*-

def traduit(chaine):
    """cette méthode vérifie la validité de la chaîne en fonction de sa provenance et, au besoin, transforme des expressions possibles (10^11) en grandeur acceptée par python"""
    # on force le type chaîne pour pouvoir faire des évaluations.
    chaine=str(chaine).replace(" ","") # et retrait de tous les espaces

    #vérification de la présence d'un float correct, sinon tente des modifs.
    try : 
        chaine=float(eval(chaine))
    except : 
        chaine=chaine.replace("10^","e")
        #remplace la chaine 10^ par e
    else:
        return chaine

    try : 
        chaine=float(eval(chaine))
    except : 
        #remplace les "x" pour la multiplication
        chaine=chaine.replace("x","*")
        chaine=chaine.replace("X","*")
    else:
        return chaine

    try : 
        chaine=float(eval(chaine))
    except : 
        #remplace les "*" devant un "e"
        chaine=chaine.replace("*e","e")
    else:
        return chaine

    try : 
        chaine=float(eval(chaine))
    except : 
        #self.debug(0,u"Erreur : %s, même après les tranformations, n'est pas une expression acceptable" %chaine)
        return 1.0
    else:
        return chaine
    return 1.0

