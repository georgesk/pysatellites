licence="""
    pysatellites : a program to plot trajectories of satellites
    Copyright (C) 2007-2008 Jean-Baptiste Butet <ashashiwa@gmail.com>,
              (C) 2007-2008 Georges Khaznadar <georgesk@ofset.org>
    

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

licence_fr="""
    pysatellites : un programme pour tracer les trajectoires de satellites
    Copyright (C) 2007-2008 Jean-Baptiste Butet <ashashiwa@gmail.com>,
              (C) 2007-2008 Georges Khaznadar <georgesk@ofset.org>
    
    Ce projet est un logiciel libre : vous pouvez le redistribuer, le modifier selon les terme de la GPL (GNU Public License) dans les termes de la Free Software Foundation concernant la version 3 ou plus de la dite licence.
    
    Ce programme est fait avec l'espoir qu'il sera utile mais SANS AUCUNE GARANTIE. Lisez la licence pour plus de détails.
    
    <http://www.gnu.org/licenses/>.
"""

import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from debug import Debug
from mainWindow import StartQT5

                

        
def usage():
    print ("Usage : pysatellites [-h | --help] [-d n | --debug=n] [-f fichier | --fichier=fichier]")
    sys.exit(0)

def run():
    app = QApplication(sys.argv)

    #translation
    #locale = QLocale.system().name()

    #qtTranslator = QTranslator()
    #if qtTranslator.load("qt_" + locale):
    #    app.installTranslator(qtTranslator)
    #appTranslator = QTranslator()
        
   # if appTranslator.load("lang/pyfocus_" + locale):
        #app.installTranslator(appTranslator)

    from getopt import getopt
    optlist, argv=getopt(sys.argv[1:],"d:f:h",["debug=","fichier=","help"])

    debugger=Debug(0)
    for (cle,val) in optlist:
        if cle=="-d" or cle=="--debug":
            debugger=Debug(int(val))
        if cle=="-h" or cle=="--help":
            usage()
        if cle=="-f" or cle=="--fichier":
            print ("On ne sait pas encore quoi faire de '%s', la fonctionnalité gouvernée par '%s' reste à implémenter." %(val,cle))

    windows = StartQT5(None, debugger=debugger, app=app)
    windows.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    run()
