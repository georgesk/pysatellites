DESTDIR = 

STELLARIUM_TEXTURES = /usr/share/stellarium/textures

all: user-interface pysatellites.1

pysatellites.1: manpage.xml
	xsltproc --nonet /usr/share/sgml/docbook/stylesheet/xsl/nwalsh/manpages/docbook.xsl manpage.xml

clean:
	rm -f *~ *.pyc
	rm -rf __pycache__
	rm -f UI_*
	rm -rf build

user-interface: UI_pysat.py UI_graphe.py

UI_%.py: %.ui
	pyuic5 $< -o $@

install: all
	mkdir -p $(DESTDIR)/usr/bin
	install -m 755 pysatellites $(DESTDIR)/usr/bin
	mkdir -p $(DESTDIR)/usr/share/applications
	install -m 644 pysatellites.desktop $(DESTDIR)/usr/share/applications
	mkdir -p $(DESTDIR)/usr/share/pysatellites
	install -m 644 *.py $(DESTDIR)/usr/share/pysatellites
	cp -a icones $(DESTDIR)/usr/share/pysatellites
	# install images from stellarium-data;
	# stellarium-data is not necessary later.
	mkdir -p $(DESTDIR)/usr/share/pysatellites/images
	for d in $(STELLARIUM_TEXTURES); do \
	  for f in $$(ls $$d/*.png); do \
	    g=$(DESTDIR)/usr/share/pysatellites/images/$$(echo $$(basename $$f)| sed -e 's/png/jpg/'); \
	    convert $$f $$g; \
	  done; \
	done


.PHONY = user-interface install clean install-textures all install-for-debian
