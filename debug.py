# -*- coding: utf-8 -*-

class Debug:
    def __init__(self, debugLevel):
        self.debugLevel=debugLevel

    def __call__(self,level,msg):
        if self.debugLevel > level:
            print(msg)
