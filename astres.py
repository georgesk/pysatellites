# -*- coding: utf-8 -*-
"""La plupart de ces données ont été adaptées à partir de la version
   anglaise de Wikipedia : http://en.wikipedia.org
"""

from flottant import traduit

astreNom=[
    # nom_local, nom_stellarium, masse_kg, rayon_km, commentaire, jour, flipped
    # flipped fait référence à une propréiété dans le fichier Planet.cpp
    # de src/libplanet du logiciel xplanet.
    
    ["Terre","earth-clouds","6x10^24","6400","Planète du système solaire","1","1"],
    ["Soleil","sun","6x10^31","695000","Étoile du système solaire","26","1"],
    ["Lune","moon","7.33x10^22","1740","Lune de Terre","27.3216","1"],
    ["Amalthée","amalthea","2.08x10^18","83.5","Lune de Jupiter","0.49817943","-1"],
    ["Callisto","callisto","1.076x10^23 ","2410","Lune de Jupiter","16.6890184","-1"],
    ["Deimos","deimos","1.48x10^15","6.2","Lune de Mars","1.26244","-1"],
    ["Dione","dione","1.1x10^21","561","Lune de Saturne","2.736915","-1"],
    ["Encelade","enceladus","1.08x10^20","252","Lune de Saturne","1.370218","-1"],
    ["Épiméthée","epimetheus","57","5.3x10^17","Lune de Saturne","0.694333517","-1"],
    ["Europe","europa","4.80x10^22","1569","Lune de Jupiter","3.551181","-1"],
    ["Ganymède","ganymede","1.4819x10^23","2634","Lune de Jupiter","7.15455296","-1"],
    ["Hyperion","hyperion","0.558x10^19","280","Lune de Saturne","21.27661","-1"],
    ["Iapète","iapetus","1.80x10^21","1450","Lune de Saturne","79.3215","-1"],
    ["Io","io","8.9319x10^22","1821.3","Lune de Jupiter","1.769137786","-1"],
    ["Janus","janus","1.91x10^18","173","Lune de Saturne","0.694660342","-1"],
    ["Jupiter","jupiter","1.90x10^27 ","70x10^3","Planète du système solaire","9.925/24","-1"],
    ["Mars","mars","6.4185x10^23","3390","Planète du système solaire","1.025957","-1"],
    ["Mercure","mercury","3.3022x10^23","2440","Planète du système solaire","58.646","-1"],
    ["Mimas","mimas","3.7493x10^19","390","Lune de Saturne","0.9424218 ","-1"],
    ["Miranda","miranda","6.59x10^19","470","Lune d'Uranus","1.413479","1"],
    ["Neptune","neptune","1.0243x10^26","24750","Planète du système solaire","0.6713","-1"],
    ["Obéron","oberon","3.014x10^21","761.4","Lune d'Uranus","13.463234","1"],
    ["Phobos","phobos","1.07x10^16","11.1","Lune de Mars","0.318 910 23","-1"],
    ["Pluton","pluto","1.30x10^22","1195","Planète du système solaire","-6.387230","1"],
    ["Prométhée","prometheus","1.566x10^17","100","Lune de Saturne","0.612990038","-1"],
    ["Protée","proteus","4.4x10^19","410","Lune de Neptune","1.12231477","-1"],
    ["Rhéa","rhea","2.3065x10^21","1525","Lune de Saturne","4.518212","-1"],
    ["Saturne","saturn","5.6846x10^26","60x10^3","Planète du système solaire","0.445","-1"],
    ["Tethys","tethys","6.174x10^20","1060","Lune de Saturne","1.887802","-1"],
    ["Titan","titan","1.345x10^23","2576","Lune de Saturne","15.945","-1"],
    ["Triton","triton","2.14x10^22","1353","Lune de Neptune","-5.877","-1"],
    ["Umbriel","umbriel","1.2x10^21","1169","Lune d'Uranus","4.144","1"],
    ["Vénus","venus","4.8685x10^24","6051","Planète du système solaire","-243.0185","1"]
    ]

class Astre:
    def __init__(self,cle):
        for a in astreNom:
            if a[1]==cle: break
        self.nom=a[0]
        self.cle=cle
        self.masse=traduit(a[2])
        self.rayon=1000*traduit(a[3])
        self.commentaire=a[4]
        self.rotationSiderale=a[5] #unité jour
        self.flip=a[6]
