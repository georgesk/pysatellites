# -*- coding: utf-8 -*-

"""
video.py est un module permettant de faire un fichier video à partir d'un
tableau de positions-vitesses tel que pysatellite peut le réaliser.
"""

import math, datetime, tempfile, os.path, os
from astres import Astre
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from threading import *
from debug import Debug
import flottant as flottant

class Cinema(Thread):
    def __init__(self,repertoire, astre, dateorigine, liste_temps, liste_pos, geometrie, pas=1, nettoie=True, boum=-1.0, debugger=Debug(0)):
        Thread.__init__(self)
        self.repertoire=repertoire
        self.astre=astre
        self.dateorigine=dateorigine
        self.liste_temps=liste_temps
        self.liste_pos=liste_pos
        self.geometrie=geometrie
        self.pas=pas
        self.nettoie=nettoie
        self.boum=boum
        self.dir=tempfile.mkdtemp("","pysat")
        self.fini=False
        self.nbImage=0
        self.debug=debugger
        
    def run(self):
        self.video()
        os.system("vlc --loop %s > /dev/null 2>&1" %os.path.join(self.dir,"out.avi"))
        if self.nettoie: os.system("rm -r %s" %self.dir)
        
    def video(self):
        self.images()
        cmd="ffmpeg -r 25 -f image2 -i %s -f avi -vcodec mpeg1video -b 800k %s > /dev/null 2>&1" %(os.path.join(self.dir, "%04d.jpg"), os.path.join(self.dir, "out.avi"))
        os.system(cmd)

    def images(self):
        listenum= range(0, len(self.liste_temps), self.pas)
        for i in listenum:
            if self.fini:
                return
            temps=self.liste_temps[i]
            if self.boum > 0 and temps > self.boum:
                self.imageCrash(self.nbImage)
                self.fini=True
                return
            x=self.liste_pos[i][0]
            y=self.liste_pos[i][1]
            z=self.liste_pos[i][2]
            cmd=self.xplanetCmd(temps, x, y, z,
                                os.path.join(self.dir, "%04d.jpg" %self.nbImage))
            os.system(cmd)
            self.nbImage+=1

    def imageCrash(self,num):
        for n in range(20):
            nomfichier=os.path.join(self.dir, "%04d.jpg" %(num+n))
            (w,h)=self.geometrie.split("x")
            img=QImage(int(w), int(h), QImage.Format_RGB32)
            img.fill(QColor("red").rgb())
            img.save(nomfichier)
    
    def xplanetCmd(self,temps, x, y, z, nomfichier):
        """
        dateorigine est un objet datetime fixe,
        temps est une durée en seconde
        """
        a=Astre(self.astre)
        td=datetime.timedelta(seconds=temps)
        date=self.dateorigine+td
        dateXplanet=date.strftime("%Y%m%d.%H%M%S")

        rayon=1.0*(x**2+y**2+z**2)**0.5
        range=rayon/a.rayon
        if range > 1.0:
            radius=100*math.asin(1.0/range)
        else:
            radius=9000
        longitude0=360.0*temps/(24*3600*flottant.traduit(a.rotationSiderale))
        longitude=180/math.pi*math.atan2(y,x)-90
        latitude=180/math.pi*math.atan2(z,(x**2+y**2)**0.5)
        cmd="xplanet -date %s -radius %s -num_times 1 -output '%s' -geometry %s -origin %s -range %s -longitude %s -latitude %s -starmap BSC -searchdir %s -body %s -label >/dev/null 2>&1" %(dateXplanet, radius, nomfichier, self.geometrie, self.astre, range, int(a.flip)*(longitude-longitude0), latitude, self.repertoire.chemin("textures"), self.astre)
        self.debug(4,u"Lancemende de «%s»" %cmd)
        return cmd
    
