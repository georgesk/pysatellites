#-*- coding: utf-8 -*-

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class Point(QLabel):
    def __init__(self, parent, point, color, numero, app, pred=None,type_de_point="petit"):
        """
        Crée un point graphique. Paramètres :
        parent : widget parent
        point : coordonnées (de type vecteur)
        color : couleur
        numero : numéro à afficher
        app : l'application qui commande
        pred : le point prédecesseur
        type_de_point : un paramètre de style
        """
        QLabel.__init__(self, parent)
        self.app=app
        self.point, self.color = point,color
        #self.setGeometry(QRect(0,0,640,480))
        self.setGeometry(QRect(0,0,parent.width(),parent.height()))
        self.numero=numero
        self.type_de_point = type_de_point
        if type_de_point=="petit" :
            self.largeur=2
        elif type_de_point=="gros" :
            self.largeur=4
        else :
            self.largeur=2

    def icone(self,nom):
        return self.app.rep.fichier("icones",nom)
    
    def paintEvent(self,event):
        self.painter = QPainter()
        self.painter.begin(self)
        self.painter.setPen(QColor(self.color))
        self.painter.translate(self.point[0], self.point[1])
        if self.type_de_point=="boum" :
            self.image_sat=QPixmap(self.icone("sat_mini_boum.png"))
            self.painter.drawPixmap(0,0,self.image_sat)
        elif self.type_de_point=="gros" :
            self.image_sat=QPixmap(self.icone("sat_mini.png"))
            self.painter.drawPixmap(0,0,self.image_sat)
            self.painter.drawLine(-self.largeur,0,self.largeur,0)
            self.painter.drawLine(0,-self.largeur,0,self.largeur)
        elif self.type_de_point=="petit" :
            self.painter.drawLine(-self.largeur,0,self.largeur,0)
            self.painter.drawLine(0,-self.largeur,0,self.largeur)
        
        self.painter.end()
