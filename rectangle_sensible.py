# -*- coding: utf-8 -*-

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from debug import Debug

class Rs(QWidget):
    def __init__(self, parent, geometry=None,
                 image=None, text=None,
                 color=QColor("grey"),
                 debuglevel=0,
                 onPress=None,
                 onRelease=None,
                 onDoubleClick=None,
                 onMove=None,
                 debugger=Debug(0)):
        QWidget.__init__(self,parent)
        if geometry==None: # le widget recouvrira le parent
            if parent!=None:
                self.setGeometry(QRect(0,0,parent.width(),parent.height()))
            else:
                self.setGeometry(QRect(0,0,100,100))
        else:
            self.setGeometry(geometry)
        self.setMouseTracking(True)
        self.image=image
        self.text=text
        self.color=color
        self.debug=debugger
        self.onPress=onPress
        self.onRelease=onRelease
        self.onDoubleClick=onDoubleClick
        self.onMove=onMove
        
    def mousePressEvent(self,ev):
        if self.onPress != None:
            return self.onPress(ev)
        else:
            self.debug(9, "event onPress still to implement")
    def mouseReleaseEvent(self,ev):
        if self.onRelease != None:
            return self.onRelease(ev)
        else:
            self.debug(9, "event onRelease still to implement")
    def mouseMoveEvent(self,ev):
        if self.onMove != None:
            return self.onMove(ev)
        else:
            self.debug(9, "event onMove still to implement ... x=%s, y=%s" %(ev.x(),ev.y()))
    def mouseDoubleClickEvent(self,ev):
        if self.onDoubleClick != None:
            return self.onDoubleClick(ev)
        else:
            self.debug(9, "event onDoubleClick still to implement")
        
    def paintEvent(self, event):
        QWidget.paintEvent(self,event)
        self.painter = QPainter()
        self.painter.begin(self)
        if self.color !=None:
            self.painter.fillRect(QRect(0,0,self.width(),self.height()),
                                  self.color)
        if self.image != None:
            self.painter.drawImage(0,0,self.image)
        if self.text != None:
            self.painter.drawText(0,0,self.text)
        self.painter.end()

class RsImage(Rs):
    """Une classe dérivée du rectangle sensible Rs, qui contient
    une image et se place en x,y sur le parent
    """
    def __init__(self, parent, x, y, image,
                 text=None,
                 color=QColor("grey"),
                 onPress=None,
                 onRelease=None,
                 onDoubleClick=None,
                 onMove=None,
                 debugger=Debug(0)):
        rect=QRect(x,y,image.size().width(), image.size().height())
        Rs.__init__(self, parent, rect, image=image,
                    text=None,
                    color=color,
                    onPress=onPress,
                    onRelease=onRelease,
                    onDoubleClick=onDoubleClick,
                    onMove=onMove,
                    debugger=debugger)
