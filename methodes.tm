<TeXmacs|1.0.6.11>

<style|article>

<\body>
  <doc-data|<doc-title|M�thodes utilis�es dans le logiciel \S pysatellites
  \T>|<doc-author-data|<author-name|Georges Khaznadar
  >|<author-email|georgesk@ofset.org>>>

  <section|Utilit� du logiciel \S pysatellites \T>

  Le logiciel pysatellites sert � simuler le lancement de satellites autour
  de diverses plan�tes. En France, ce logiciel est utilis� dans
  l'enseignement au niveau du lyc�e. L'�l�ve est invit� � choisir une
  plan�te, ou � pr�ciser les param�tres de rayon et de masse qu'il veut, puis
  il contr�le le point de lancment d'un satellite, sa vitesse radiale et sa
  vitesse orthoradiale. Quand ce choix est fini, il lance la simulation et
  voit quelle trajectoire le satellite peut alors suivre.

  <section|M�thode utilis�e pour la simulation>

  La m�thode est une m�thode de calcul de proche en proche : � des
  intervalles de temps r�guliers, la vitesse et la position du satellite
  connues sont utilis�es afin de pr�dire sa position et sa vitesse un
  intervalle de temps plus tard. On parle d'int�gration num�rique, car seule
  la loi locale qui donne la force d'attraction appliqu�e au satellite est
  prise en consid�ration.

  Un autre m�thode serait possible : dans le cas d'un probl�me � un corps
  plong� dans un potentiel newtonien, les �quations de la dynamique du
  satellite admettent des solutions alg�briques que l'on sait d�terminer.
  J'ai utilis� un document synth�tique publi� sur Internet, � l'adresse\ 

  <code*|http://melusine.eu.org/syracuse/immae/mpsi/physique-chimie/mecanique/08.pdf>

  Ce document r�sume ce qu'on peut retenir comme propri�t� des coniques
  (ellipses, parabole, hyperboles), et la solution connue du probl�me � un
  corps dans un potentiel newtonien. On peut l'utiliser pour calculer sans
  avoir � terminer la simulation divers param�tres. L'un d'entre eux est tr�s
  important, il s'agit de la p�riode <math|T> du mouvement quand l'�nergie
  m�canique <math|E<rsub|m>> du satellite est n�gative, et que celui-ci
  d�crit une ellipse dans le puits de potentiel de l'astre qui l'attire.

  <section|La m�thode d'int�gration de Runge-Kutta>

  <section|D�termination de la p�riode d'un mouvement elliptique>

  On conna�t la distance <math|r> du satellite � l'astre de masse <math|M>.
  On en d�duit facilement son �nergie potentielle massique,
  <math|E<rsub|p>/m=-<frac|GM|r>>, o� <math|G=6,67.10<rsup|-11>u.s.i.> est la
  constante universelle de gravitation. Connaissant sa vitesse radiale
  <math|<wide|r<with|mode|text|<math|>>|\<dot\>>> et sa vitesse orthoradiale
  <math|r<wide|\<theta\>|\<dot\>>>, on d�duit son �nergie cin�tique massique,
  <math|E<rsub|c>/m=<with|mode|text|<math|<wide|r|\<dot\>>>^2>+(<with|mode|text|<math|r<wide|\<theta\>|\<dot\>>>>)<rsup|2>>.
  Il suffit d'aditionner les �nergies pour parvenir � l'�nergie m�canique
  massique, <math|E<rsub|m>/m=-<frac|GM|r>+<with|mode|text|<math|<wide|r|\<dot\>>>^2>+(<with|mode|text|<math|r<wide|\<theta\>|\<dot\>>>>)<rsup|2>>.

  Plusieurs cas se pr�sentent alors :

  <\enumerate-numeric>
    <item><math|E<rsub|m>/m \<less\> 0> : le satellite reste dans le puits de
    potentiel de l'astre, sa trajectoire est une ellipse, qu'il parcourt avec
    une p�riode <math|T>.

    <item><with|mode|math|E<rsub|m>/m = 0> : le satellite n'est pas li�, il
    poss�de tout juste la vitesse de lib�ration, sa trajectoire est une
    parabole, sa vitesse s'annule � l'infini.

    <item><with|mode|math|E<rsub|m>/m \<gtr\> 0> : le satellite n'est pas
    li�, sa vitesse � l'infini est non nulle, sa trajectoire est
    hyperbolique.
  </enumerate-numeric>

  Dans le premier cas seulement, une p�riode existe pour le mouvement du
  satellite, et on la calcule ainsi : le grand axe <math|a> de l'ellipse se
  d�duit de la constante d'attraction (<math|k=GMm)> par la formule <math|a =
  -k/2Em=<frac|-GM|2(-<frac|GM|r>+<with|mode|text|<math|<wide|r|\<dot\>>>^2>+(<with|mode|text|<math|r<wide|\<theta\>|\<dot\>>>>)<rsup|2>)>><math|>.
  Connaissant le grand axe <math|a> de l'ellipse, on peut alors d�terminer la
  p�riode <math|T> du mouvement gr�ce � la troisi�me loi de Kepler,
  <math|T<rsup|2>=4\<pi\><rsup|2>/MG*a<rsup|3>>, soit
  <math|T=2\<pi\><sqrt|<frac|1|MG*a<rsup|3>>|>>.

  Quand la p�riode <math|T> du mouvement est connue, on peut prendre comme
  ordre de grandeur de l'intervalle de temps pour l'int�gration, un centi�me
  de cette p�riode. �a donne des r�sultats satisfaisants pour les mouvement
  d'excentricit� faible : c'est � dire que la trajectoire appara�t facilement
  comme ferm�e � l'�cran, au pixel pr�s. Dans le cas d'ellipses fortement
  excentriques, il faut diminuer le l'intervalle de temps utilis� pour
  l'int�gration.
</body>

<\initial>
  <\collection>
    <associate|language|french>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|1>>
    <associate|auto-4|<tuple|4|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Utilit�
      du logiciel \S pysatellites \T> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>M�thode
      utilis�e pour la simulation> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>La
      m�thode d'int�gration de Runge-Kutta>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>D�termination
      de la p�riode d'un mouvement elliptique>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>